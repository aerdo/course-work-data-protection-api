module.exports = {
    HOST: "localhost",
    USER: "postgres",
    PASSWORD: "123456",
    database: "data_protection_coursework",
    dialect: "postgres",
    pool: {
        max: 5,
        min: 0
    }
};