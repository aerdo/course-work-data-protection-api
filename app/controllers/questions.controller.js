const db = require("../models");

const Questions = db.questions;

// обновление
exports.update = (req, res) => {
    const id = req.params.id;
    Questions.update(req.body, {
        where: { question_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.sendStatus(200);
            } else {
                res.status(400).send({
                    message: "Question not found."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error while updating Question occurred: " + err
            });
        });
};

// вывод вопроса для пользователя
exports.findOneByNum = (req, res) => {
    const user_id = req.params.user_id;
    const num = req.params.num;

    db.sequelize.query(`SELECT * from get_question(${user_id},${num})`).then(data => {
            if (data[0].length !== 0){
                res.send(data[0][0]);
            } else {
                res.send({
                    message: `Question not found.`
                });
            }
        })
        .catch(err => {
            res.sendStatus(404);
        });
};

// проверка правильности
exports.checkAnswer = (req, res) => {
    if (!req.body.answer) {
        res.status(400).send({
            message: "Content can not be empty."
        });
        return;
    }

    const question_id = req.params.question_id;
    const user_answer = req.body.answer;

    db.sequelize.query(`SELECT check_question(${question_id}, '${user_answer}')`).then(data=>{
        if (data[0].length !== 0){
            const result = data[0][0].check_question;
            if (result){
                res.send({
                    access: 'true'
                });
            }else{
                res.send({
                    message: 'Try again'
                });
            }

        } else {
            res.send({
                message: `Error occured.`
            });
        }
    })
        .catch(err => {
            res.status(404).send({
                message: err
            });
        });
};

exports.getAllQuestionNums = (req, res) => {
    const id = req.params.id;
    Questions.findAll({
            where: { user_id: id },
            order: [['question_id', 'ASC']],
            attributes: ['question_id']
        })
        .then(data => {
            res.status(200).send(data);
        })
        .catch(err => {
            res.status(404);
        });
}

exports.getAll = (req, res) => {
    const id =req.params.id;
    Questions.findAll({
        where: { user_id: id },
        order: [['question_id', 'ASC']],
        attributes: ['question_id', 'question_text']
    })
        .then(data => {
            res.status(200).send(data);
        })
        .catch(err => {
            res.status(404);
        });
}