const db = require("../models");

const Users = db.users;
const Op = db.Sequelize.Op;

function randomInteger(min, max) {
    // получить случайное число от (min-0.5) до (max+0.5)
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}

// вывод всех записей
exports.findAll = (req, res) => {
    Users.findAll({order: [
            ['user_name', 'ASC'],
        ],})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.sendStatus(404);
        });
};

// поиск одного пользователя по имени с получением случайного номера вопроса
exports.findOne = (req, res) => {
    const name = req.params.name;
    Users.findAll({ where: { user_name: { [Op.like]: name }}})
        .then(data => {
            if (data && data.length !== 0) {
                const temp = JSON.parse(JSON.stringify(data))[0];
                // const temp = data[0];
                // генерируем номер вопроса
                temp.question_num = randomInteger(1, temp.questions_amount);
                delete temp.questions_amount;
                res.send(temp);
            } else {
                res.status(404).send({
                    message: "Cannot find User."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Error retrieving User."
            });
        });
};

// создание пользователя с уникальным именем (имя без пробелов)
exports.create = (req, res) => {
    if (!req.body.user_name) {
        res.status(400).send({
            message: "Content can not be empty."
        });
        return;
    }

    const user_name = req.body.user_name;

    if (/\s/.test(user_name)){
        res.status(400).send({
            message: "No whitespaces in username."
        });
        return;
    }

    const user = {
        user_name: req.body.user_name,
        questions_amount: req.body.questions_amount,
        attempts_amount: req.body.attempts_amount
    };

    Users.create(user)
        .then(data => {
            res.status(201).send({
                status: "Created"
            })
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        });
};

// обновление
exports.update = (req, res) => {
    const id = req.params.id;

    Users.update(req.body, {
        where: { user_id: id },
        attributes: ['questions_amount', 'attempts_amount']
    })
        .then(num => {
            if (num == 1) {
                res.status(200).send({
                    status: "updated"
                })
            } else {
                res.status(400).send({
                    message: `Cannot update User.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error while updating User occurred: " + err
            });
        });
};

// удаление
exports.delete = (req, res) => {
    const id = req.params.id;
    Users.destroy({
        where: { user_id: id }
    })
        .then(num => {
            if (num == 1) {
                res.status(200).send({
                    status: "deleted"
                })
            } else {
                res.send({
                    message: "Cannot delete User."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error while deleting User occurred: " + err
            });
        });
};

exports.getQuestionsAmount = (req, res) => {
    const id = req.params.user_id;

    Users.findByPk(id, {attributes: ['questions_amount']}).then(data => {
            if (data.questions_amount){
                res.send(data);
            }

        })
        .catch(err => {
            res.sendStatus(404);
        });
}

exports.updateName = (req, res) => {
    const id = req.params.id;
    const user_name = req.body.user_name;

    if (/\s/.test(user_name)){
        res.status(400).send({
            message: "No whitespaces in username."
        });
        return;
    }

    Users.update(req.body, {
        where: { user_id: id },
        attributes: ['user_name']
    })
        .then(num => {
            if (num == 1) {
                res.status(200).send({
                    status: "updated"
                })
            } else {
                res.status(400).send({
                    message: `Cannot update User.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the User."
            });
        });
}

exports.checkConnection = (req, res) => {
    res.status(200).send({
        status: "OK"
    });
}