const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const dataTypes = require("sequelize");

/*const sequelize = new Sequelize(dbConfig.database, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min
    }
});*/


const sequelize = new Sequelize(process.env.DATABASE_URL, {
    dialectOptions: {
        ssl: {
            require: true,
            rejectUnauthorized: false
        }
    }
});



try {
    sequelize.authenticate().then(()=> {
        console.log('Connection has been established successfully.')
    }).catch((res)=>{
        console.log("Unable to connect to data base. More: " + JSON.stringify(res))
    })
} catch (error) {
    console.error('Error: ', error);
}

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require('./users.model')(sequelize, dataTypes);
db.questions = require('./questions.model')(sequelize, dataTypes);

module.exports = db;