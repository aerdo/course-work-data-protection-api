module.exports = (sequelize, dataTypes) => {
    const Questions = sequelize.define('questions', {
            question_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                type: dataTypes.INTEGER
            },
            question_text: {
                type: dataTypes.STRING
            },
            answer_text:{
                type: dataTypes.STRING
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
    return Questions;
};