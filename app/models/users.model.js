module.exports = (sequelize, dataTypes) => {
    const Users = sequelize.define('users', {
            user_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_name: {
                type: dataTypes.STRING
            },
            questions_amount: {
                type: dataTypes.STRING
            },
            attempts_amount:{
                type: dataTypes.STRING
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
    return Users;
};