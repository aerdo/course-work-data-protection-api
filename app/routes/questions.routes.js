const questions = require("../controllers/questions.controller");
module.exports = app => {
    var router = require("express").Router();

    // первый вход
    router.get("/users/:id/questions", questions.getAllQuestionNums) // получение номеров вопросов для первого входа

    // возможности пользователя
    router.get("/users/:id/questions/full", questions.getAll) // получение вопросов с номерами и содержанием
    router.put("/questions/:id", questions.update); // обновление вопросов и ответов

    // логин
    router.get("/login/get_question/:user_id/:num", questions.findOneByNum); // + получение вопроса для входа
    router.post("/login/check_answer/:question_id", questions.checkAnswer); // + проверка ответа на вопрос

    app.use('/api', router);
};