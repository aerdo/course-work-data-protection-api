const users = require("../controllers/users.controller");
module.exports = app => {
    var router = require("express").Router();

    router.get("/check", users.checkConnection);

    router.put("/users/rename/:id", users.updateName);
    // логин
    router.get("/login/:name", users.findOne); //

    // первый вход
    router.get("/login/get_questions_amount/:user_id", users.getQuestionsAmount);

    // права админа
    router.get("/users", users.findAll); // вывод всех пользователей
    router.post("/users", users.create); // создание пользователя
    router.put("/users/:id", users.update); // изменение данных пользователя (обычный пользователь может изменить только имя)
    router.delete("/users/:id", users.delete); // удаление пользователя


    app.use('/api', router);
};