--------------------[Создание таблиц]-----------------------

-- Пользователь
CREATE TABLE Users
(
	user_id SERIAL PRIMARY KEY,
	user_name varchar(100),
	questions_amount integer,
	attempts_amount integer
);

-- Вопросы и ответы
CREATE TABLE Questions
(
	question_id SERIAL PRIMARY KEY,
	user_id integer,
	question_text varchar(256),
	answer_text varchar(256)
);

-------------------- [Ограничения]--------------------------

alter table Users add check (user_name is not null); -- имя пользователя не должно быть пустым
alter table Users add check (questions_amount between 1 and 10); -- от 1 до 15 вопросов
alter table Users add check (attempts_amount between 1 and 5); -- от 1 до 5 попыток ввода

alter table Questions add check (user_id is not null);
alter table Questions add check (question_text is not null);
alter table Questions add check (answer_text is not null);

alter table questions add foreign key (user_id) references users(user_id) on update no action; -- нельзя изменить id пользователей, если у них есть вопросы

--------------------[Триггеры]------------------------------

-- проверка на уникальность имени при создании пользователя
create function check_unique_username_fun() 
	returns trigger as
$$
begin 
	if (new.user_name in (select user_name from users)) then raise exception 'Username has to be unique';
	end if;
	return new;
end;
$$ language plpgsql;

create trigger check_unique_username
	before insert
	on users
	for each row
execute procedure check_unique_username_fun();

------------------------------------------------------------
-- при добавлении пользователя автоматичски создаются экземпляры его вопросов
create function add_questions_fun() 
	returns trigger as
$$
declare 
	n integer := (select questions_amount from users where user_id = New.user_id);
begin 
	for i in 1..n loop
		insert into questions (user_id, question_text, answer_text) values (New.user_id, '-', '-');
	end loop;
	return new;
end;
$$ language plpgsql;

create trigger add_questions
	after insert
	on users
	for each row
execute procedure add_questions_fun();

------------------------------------------------------------
-- проверка на уникальность имени при изменении пользователя
create function check_unique_username_update_fun() 
	returns trigger as
$$
begin 
	if (new.user_name in (select user_name from users where user_id <> old.user_id)) then raise exception 'Username has to be unique';
	end if;
	return new;
end;
$$ language plpgsql;

create trigger check_unique_username_update
	before update
	on users
	for each row
execute procedure check_unique_username_update_fun();

------------------------------------------------------------
-- при удалении пользователя автоматически удаляются его вопросы
-- запрет на удаление админа
create function delete_user_cascase_fun() 
	returns trigger as
$$
begin 
	if (old.user_id = 1) then raise exception 'Cannot delete admin';
	end if;
	delete from questions where questions.user_id = old.user_id;
	return old;
end;
$$ language plpgsql;

create trigger delete_user_cascase
	before delete
	on users
	for each row
execute procedure delete_user_cascase_fun();

------------------------------------------------------------
-- при изменении количества вопросов админом экземпляры вопросов автоматически добавятся или удалятся лишние вопросы
create function check_questions_delete_fun() 
	returns trigger as
$$
declare 
	n integer := count(*) from questions where questions.user_id = new.user_id;
	m integer := new.questions_amount;
	k integer := m - n;
begin 
	if (k <> 0) then 
		if (k < 0) then
			delete from questions where question_id in 
			(select questions.question_id from questions 
			where user_id = new.user_id order by question_id desc limit abs(k));
		else
			for i in 1..k loop
				insert into questions (user_id, question_text, answer_text) values (New.user_id, '?', '?');
			end loop;
		end if;
	end if;
	return new;
end;
$$ language plpgsql;

create trigger check_questions_delete
	after update
	on users
	for each row
execute procedure check_questions_delete_fun();

--------------------[Хранимые функции]----------------------

-- получение вопроса по номеру пользователя и случайного числа
create function get_question(userid in integer, num in integer)
	returns table(question_id integer, question_text varchar(256))
as
$$
begin
	return query
		select questions.question_id, questions.question_text
		from questions where user_id = userid
		order by question_id asc offset (num-1) limit 1;
end;
$$ language plpgsql;
	
------------------------------------------------------------
-- проверка ответа
create function check_question(questionid in integer, answ in varchar(256))
	returns boolean
as
$$
begin
	
	return answ = (select answer_text from questions where question_id = questionid);
end;
$$ language plpgsql;

--------------------[Создание администратора]---------------

insert into Users values (1,'admin', '1', '1');




