const express = require("express");
const cors = require("cors");

var corsOptions = {
    //origin: "http://localhost:3000"
    origin: "https://data-protection-coursework.herokuapp.com"
};

const app = express();

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.sendStatus(200);
})


require('./app/routes/users.routes')(app);
require('./app/routes/questions.routes')(app);

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});